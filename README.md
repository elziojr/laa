# Log Access Analytics
#### This is a microservice application to get a log plain POST, convert into a legible data, send to an elasticsearch instace and get pre-defined metrics that uses Lucene core engine.
<hr>

## The problem
We need to run an application that get a lot of requests and sumarize some metrics near a real time. But, the core application cannot use Spring and Docker.

## The Resolution
Make an automatic coded infrastructure that will do:
- Create a s3 bucket and send the application runnable to s3 into a s3BucketObject
- Create a VPC and Security Groups configs, to link all machines
- Create an ALB to put in front my backEnd, making a rounding robbin to send requests to backend
- Create an Amazon ElasticSearch Domain to post all data
- Get the elasticSearch host from created instance and build 3 ec2 instances pointing to elasticsearch host
- On ec2 instances get the s3 application runnable and run the application
- On ec2 instances creation creates the target group on ALB to linking the ingress traffic to that ec2 instances

Resolution overview: <br>
![](./overview.png)


#### **- PULUMI**
A tool to code Modern Infrastructure as a Code, with this tool I can automatize the creation, deploy and management the cloud environment

#### **- MICRONAUT**
A light-weight and powerfull Spring alternative, to do my application management and REST layer.

#### **- ELASTICSEARCH**
To centralize ours logs and provide a powerful tool to get the metrics near real-time.
<br><br>

## HOW TO USE

- Have an AWS account to deploy the infrastructure
- Build the runnable jar `./gradlew clean build`
- Create an IAM User with admin roles to be the pulumi deployer
- Define aws credetials local file:
    - `cd ~`
    - `mkdir .aws`
    
    - then create your pulumi deployer credentials file
    ```
      cat > ~/credentials <<EOF
       [default]
       aws_access_key_id = AKIASONT7....  (your deploy user keyId)
       aws_secret_access_key = LT+tSRe+gzw/jGtt0DlNbtGL.... (your deploy user accessKey)
       EOF
    ```
- Install pulumi `curl -fsSL https://get.pulumi.com | sh`
- Go to project environment dir `cd laa/environment`
- Run `pulumi up`

Then Pulumi will deploy all infrastructure and backend server, and then it will log in your console the output in a line like this: <br>
```
Outputs:
   ~ endpoint: "laaAlb-24be320-1230275486.us-east-1.elb.amazonaws.com" 
```
ps: if elasticsearch have a long time to deploy maybe you will get a errro, just run `pulumi up` again. 
<br><br>
That's it!! <br><br>
Now you get the application running in your amazon cloud, just get the output endpoint in port 8080 and make the POST with url access on <br>
`yourEndpointOutput:8080/ingest` to send to elasticSearch data. <br>

An example of data:
```
/pets/exotic/cats/10 1037825323957 5b019db5-b3d0-46d2-9963-437860af707f 1
/pets/guaipeca/dogs/1 1037825323957 5b019db5-b3d0-46d2-9963-437860af707g 2
/tiggers/bid/now 1037825323957 5b019db5-b3d0-46d2-9963-437860af707e 3
```

And you can get the metrics over the requests on <br>
`yourEndpointOutput:8080/metrics` 