package log.access.analytics.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultWithCount {

    private String value;
    private Integer count;
    
}
