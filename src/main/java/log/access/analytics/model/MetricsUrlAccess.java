package log.access.analytics.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class MetricsUrlAccess {

    private List<ResultWithCount> top3AllAroundWorld;
    private Map<ResultWithCount, List<ResultWithCount>> top3PerRegion;
    private List<ResultWithCount> urlLessAccess;
    private List<ResultWithCount> minuteWithMoreAccess;

}