package log.access.analytics.model;

public enum Metrics {
    TOP_3_VISITED_URLS,
    TOP_3_VISITED_URLS_BY_REGION,
    TOP_URL_LESS_ACCESS,
    MINUTE_WITH_MORE_ACCESS
}
