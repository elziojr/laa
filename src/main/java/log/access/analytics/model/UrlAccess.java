package log.access.analytics.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UrlAccess {

    private String url;
    private LocalDateTime visitedAt;
    private String uuidUser;
    private Integer region;

}
