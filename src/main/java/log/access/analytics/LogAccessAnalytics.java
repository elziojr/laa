package log.access.analytics;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(info = @Info(
        title = "laa",
        version = "1.0"
))
public class LogAccessAnalytics {

    public static void main(String[] args) {
        Micronaut.run(LogAccessAnalytics.class);
    }
}