package log.access.analytics.properties;

import io.micronaut.context.annotation.Value;
import lombok.Data;

import javax.inject.Singleton;

@Data
@Singleton
public class LogAccessAnalyticsProperties {

    @Value("${micronaut.elasticsearch.host}")
    private String elasticSearchHost;

    @Value("${micronaut.elasticsearch.port}")
    private Integer elasticSearchPort;

}
