package log.access.analytics.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import log.access.analytics.model.MetricsUrlAccess;
import log.access.analytics.service.UrlAccessElasticSearchService;

import javax.inject.Inject;

@Controller
public class UrlAccessController {

    @Inject
    private UrlAccessElasticSearchService urlAccessElasticSearchService;

    @Post("/ingest")
    @Consumes(MediaType.TEXT_PLAIN)
    public HttpResponse postIngest(@Body String urlAccessLog) {
        urlAccessElasticSearchService.convertAndSendToElasticSearch(urlAccessLog);
        return HttpResponse.accepted();
    }

    @Get("/metrics")
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<MetricsUrlAccess> getMetrics() {
        MetricsUrlAccess result = urlAccessElasticSearchService.getMetricsFromElasticSearch();
        return HttpResponse.ok(result);
    }

}
