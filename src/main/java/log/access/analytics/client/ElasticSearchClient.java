package log.access.analytics.client;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.micronaut.context.annotation.Prototype;
import log.access.analytics.model.UrlAccess;
import log.access.analytics.properties.Constants;
import log.access.analytics.properties.LogAccessAnalyticsProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Slf4j
@Prototype
public class ElasticSearchClient {

    @Inject
    private LogAccessAnalyticsProperties logAccessAnalyticsProperties;

    private final String PROTOCOL = "http";

    public void sendMetricsToElasticSearch(UrlAccess urlAccess) {
        String jsonUrlAccess = new Gson().toJson(urlAccess);
        IndexRequest request = new IndexRequest(Constants.ACCESS).source(jsonUrlAccess, XContentType.JSON);
        log.info(jsonUrlAccess);

        try {
            getConnection().index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JsonObject getResultsFromMetrics(String metric) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .header("Content-Type", "application/json")
                    .uri(getHttpConnectionToSearch())
                    .POST(HttpRequest.BodyPublishers.ofString(metric))
                    .build();

            HttpClient client = HttpClient.newBuilder().build();
            String result = client
                    .send(request, HttpResponse.BodyHandlers.ofString())
                    .body();

            return (JsonObject) new JsonParser().parse(result);
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonObject();
        }
    }

    private RestHighLevelClient getConnection() {
        final String elasticHost = logAccessAnalyticsProperties.getElasticSearchHost();
        final Integer elasticPort = logAccessAnalyticsProperties.getElasticSearchPort();

        RestClientBuilder builder = RestClient
                .builder(new HttpHost(elasticHost, elasticPort, PROTOCOL));

        return new RestHighLevelClient(builder);
    }

    private URI getHttpConnectionToSearch () {
        final String elasticHost = logAccessAnalyticsProperties.getElasticSearchHost();
        final Integer elasticPort = logAccessAnalyticsProperties.getElasticSearchPort();

        return URI.create(
                String.format("%s://%s:%s/_search", PROTOCOL, elasticHost, elasticPort)
        );
    }
}
