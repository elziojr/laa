package log.access.analytics.converter;

import io.micronaut.context.annotation.Prototype;
import log.access.analytics.model.UrlAccess;

import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

@Prototype
public class UrlAccessConverter {

    private final String DELIMITER = "|";

    public List<UrlAccess> from(String urlAcessLog) {
        List<UrlAccess> listUrlAccess = new ArrayList<>();

        List<String> listLines = Arrays.asList(urlAcessLog.split(System.getProperty("line.separator")));
        listLines.forEach(line -> listUrlAccess.add(getUrlAccessFromLine(line)));

        return listUrlAccess;
    }

    public UrlAccess getUrlAccessFromLine(String line) {
        line = line.replaceAll("\\s", DELIMITER);

        String url = line.substring(0, getDelimiterIndex(line, 1));
        String timeStamp = line.substring(getDelimiterIndex(line, 1) + 1, getDelimiterIndex(line, 2));
        String uuidUser = line.substring(getDelimiterIndex(line, 2) + 1, getDelimiterIndex(line, 3));
        Integer region = Integer.valueOf(line.substring(getDelimiterIndex(line, 3) + 1));

        LocalDateTime visitedAt = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(Long.valueOf(timeStamp)), TimeZone.getDefault().toZoneId()
        );

        return UrlAccess.builder()
            .url(url)
            .visitedAt(visitedAt)
            .uuidUser(uuidUser)
            .region(region)
            .build();
    }

    private Integer getDelimiterIndex(String line, Integer occurrence) {
        return StringUtils.ordinalIndexOf(line, DELIMITER, occurrence);
    }
}
