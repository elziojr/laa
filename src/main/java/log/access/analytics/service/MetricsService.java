package log.access.analytics.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import log.access.analytics.model.Metrics;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Singleton;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Singleton
public class MetricsService {

    private InputStream metricsAsStream = this.getClass().getResourceAsStream("/metrics.json");
    private String metrics;

    public String getMetrics(Metrics metricsIdentifier) {
        try {
            if(StringUtils.isEmpty(metrics)) {
                metrics = IOUtils.toString(metricsAsStream, UTF_8);
            }

            JsonObject jsonMetrics = (JsonObject) new JsonParser().parse(metrics);
            return jsonMetrics.get(metricsIdentifier.toString()).toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}
