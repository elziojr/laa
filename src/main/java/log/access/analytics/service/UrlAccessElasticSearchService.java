package log.access.analytics.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.micronaut.context.annotation.Prototype;
import log.access.analytics.client.ElasticSearchClient;
import log.access.analytics.converter.UrlAccessConverter;
import log.access.analytics.model.Metrics;
import log.access.analytics.model.MetricsUrlAccess;
import log.access.analytics.model.ResultWithCount;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Prototype
public class UrlAccessElasticSearchService {

    @Inject
    private UrlAccessConverter urlAccessConverter;

    @Inject
    private MetricsService metricsService;

    @Inject
    private ElasticSearchClient elasticSearchClient;

    public void convertAndSendToElasticSearch(String urlAccessLog) {
        urlAccessConverter
                .from(urlAccessLog)
                .forEach(elasticSearchClient::sendMetricsToElasticSearch);
    }

    public MetricsUrlAccess getMetricsFromElasticSearch() {
        MetricsUrlAccess metricsResponse = MetricsUrlAccess.builder()
            .top3AllAroundWorld(getAggregatedResultFromMetrics(metricsService.getMetrics(Metrics.TOP_3_VISITED_URLS)))
            .top3PerRegion(getAggregatedGroupedResultsFromMetrics(metricsService.getMetrics(Metrics.TOP_3_VISITED_URLS_BY_REGION)))
            .minuteWithMoreAccess(getAggregatedResultFromMetrics(metricsService.getMetrics(Metrics.MINUTE_WITH_MORE_ACCESS)))
            .urlLessAccess(getAggregatedResultFromMetrics(metricsService.getMetrics(Metrics.TOP_URL_LESS_ACCESS)))
            .build();

        return metricsResponse;
    }

    private List<ResultWithCount> getAggregatedResultFromMetrics(String metric) {
        List<ResultWithCount> resultWithCounts = new ArrayList<>();

        JsonObject response = elasticSearchClient.getResultsFromMetrics(metric);
        if(isEmptyHits(response)) {
            return new ArrayList<>();
        }

        response.getAsJsonObject("aggregations")
            .getAsJsonObject("result")
            .getAsJsonArray("buckets")
            .forEach(item -> {
                ResultWithCount resultWithCount = ResultWithCount.builder()
                    .value(item.getAsJsonObject().get("key").getAsString())
                    .count(item.getAsJsonObject().get("doc_count").getAsInt())
                    .build();

                resultWithCounts.add(resultWithCount);
            });

        return resultWithCounts;
    }

    private Map<ResultWithCount, List<ResultWithCount>> getAggregatedGroupedResultsFromMetrics(String metric) {
        Map<ResultWithCount, List<ResultWithCount>> mapResults = new HashMap<>();

        JsonObject response = elasticSearchClient.getResultsFromMetrics(metric);
        if(isEmptyHits(response)) {
            return new HashMap<>();
        }

        JsonArray jsonArrayAggregated = response
            .getAsJsonObject("aggregations")
            .getAsJsonObject("result")
            .getAsJsonArray("buckets");

        // TODO: refazer sem precisar entrar em innerObjects de response aggregated
        jsonArrayAggregated.forEach(item -> {
            List<ResultWithCount> listInnerResults = new ArrayList<>();
            ResultWithCount resultWithCount = ResultWithCount.builder()
                .value(item.getAsJsonObject().get("key").getAsString())
                .count(item.getAsJsonObject().get("doc_count").getAsInt())
                .build();

            item.getAsJsonObject()
                .getAsJsonObject("result")
                .getAsJsonArray("buckets")
                .forEach(innerItem -> {
                    ResultWithCount innerResultWithCount = ResultWithCount.builder()
                        .value(innerItem.getAsJsonObject().get("key").getAsString())
                        .count(innerItem.getAsJsonObject().get("doc_count").getAsInt())
                        .build();

                    listInnerResults.add(innerResultWithCount);
                });

            mapResults.put(resultWithCount, listInnerResults);
        });

        return mapResults;
    }

    private Boolean isEmptyHits(JsonObject response) {
        return response == null
                || response.getAsJsonObject("hits") == null
                || response.getAsJsonObject("hits").getAsJsonObject("total").get("value").getAsInt() == 0;
    }
}
