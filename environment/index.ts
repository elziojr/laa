import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";
import * as amiEC2 from "./resources/amiEC2";
import * as elasticsearch from "./resources/elasticSearch";
import * as s3 from "./resources/s3";
import * as pulumi from "@pulumi/pulumi";
import * as elasticSearchIndexes from "./resources/elasticSearchIndexes";



// CONFIGS
const projectFileName = "laa-0.1-all.jar";
const applicationPort = 8080;
const projectJarDir = `../build/libs/${projectFileName}`;
const ec2MachinesQuantity = 3;
const ec2InstanceType = "t2.micro";
const bucketName = "bucketForDeploys";
const awsBucketPath = ".s3.amazonaws.com";
let ec2List = [];



// VPC E SECURITY GROUPS   
const vpc = awsx.ec2.Vpc.getDefault();
const albSecurityGroup = new awsx.ec2.SecurityGroup("webSecurityGroup", {
    // Default public access on port 80
    vpc
});
const ec2SecurityGroup = new awsx.ec2.SecurityGroup("ec2SecurityGroup", {
    vpc,
    egress: [
        { protocol: "-1", fromPort: 0, toPort: 0, cidrBlocks: [ "0.0.0.0/0" ] },
    ],
    ingress: [
        { protocol: "tcp", fromPort: applicationPort, toPort: applicationPort, sourceSecurityGroupId: albSecurityGroup.id }
    ]
});
const elasticSearchSecurityGroup = new awsx.ec2.SecurityGroup("elasticSearchSecurityGroup", {
    vpc,
    ingress: [
        { protocol: "-1", fromPort: 0, toPort: 0, sourceSecurityGroupId: ec2SecurityGroup.id }
    ]
});



// ALB
const alb = new awsx.lb.ApplicationLoadBalancer("laaAlb", { securityGroups: [ albSecurityGroup ] });
const listener = alb.createListener("albWebListener", { port: applicationPort });



// S3
const bucket = s3.createInstance(bucketName);
const bucketObjectDeploy = s3.createObject('laaApp', bucket, projectJarDir, projectFileName);



vpc.publicSubnets.then(subnets => {

    // ELASTICSEARCH
    const rightSubnet = subnets[0];
    const es = elasticsearch.createInstance(elasticSearchSecurityGroup, rightSubnet);
    const allNeededVars = [ bucket.bucketDomainName, bucketObjectDeploy.key, es.endpoint ];

    pulumi.all(allNeededVars).apply(([bucketName, bucketObjectKey, esEndpoint]) => {


        // EC2 CONFIG
        const elasticHost = esEndpoint;
        const elasticPort = 80;
        const createElasticSearchIndex = elasticSearchIndexes.create(elasticHost, elasticPort);
        const bucketNormalizedName = bucketName.replace(awsBucketPath, "");
        const userData = `
            #!/bin/bash
            ${createElasticSearchIndex}
            wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz -O java11.tar.gz
            tar -zxvf java11.tar.gz
            mv jdk-* java11
            export JAVA_HOME="/java11"
            export PATH=$JAVA_HOME/bin:$PATH
            export ELASTICSEARCH_HOST=${elasticHost}
            export ELASTICSEARCH_PORT=${elasticPort}
            java -version
            aws s3 cp s3://${bucketNormalizedName}/${bucketObjectKey} app.jar
            java -jar app.jar
        `;
        

        
        // EC2 INSTANCES
        for(let i = 0; i < ec2MachinesQuantity; i++) {
            let ec2Instance = amiEC2.createInstance(
                `ec2Machine-${i}`,
                ec2InstanceType,
                ec2SecurityGroup,
                rightSubnet,
                userData
            );

            // Create targetGroups for loadBalancers, linking the ec2 in targetId
            const attach = new aws.lb.TargetGroupAttachment(`targetGroupEc2-${i}`, {
                targetId: ec2Instance.privateIp,
                targetGroupArn: alb.targetGroups[0].targetGroup.arn,
                availabilityZone: ec2Instance.availabilityZone,
                port: applicationPort,
            });

            ec2List.push(ec2Instance);
        }
    });
});


export const endpoint = listener.endpoint.hostname;