import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import { Domain } from "@pulumi/aws/elasticsearch";

const domainPolicy = (elasticsearch: Domain) => {
    return new aws.elasticsearch.DomainPolicy("elasticSearchDomainPolicy", {
        accessPolicies: pulumi.interpolate`{
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "es:*",
                    "Principal": "*",
                    "Effect": "Allow",
                    "Resource": "${elasticsearch.arn}/*"
                }
            ]
        }`,
        domainName: elasticsearch.domainName,
    });
};

export const elasticSearchDomainPolicy = domainPolicy;