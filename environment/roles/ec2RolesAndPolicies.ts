import * as aws from "@pulumi/aws";

const role = new aws.iam.Role("ec2Roles", {
    assumeRolePolicy: `{
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "ec2.amazonaws.com",
                    "Service": "es.amazonaws.com"
                },
                "Effect": "Allow",
                "Sid": ""
            }
        ]
    }`
});

const rolePolicy = new aws.iam.RolePolicy("ec2Policies", {
    role: role.id,
    policy: `{
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:Get*",
                    "s3:List*",
                    "es:*"
                ],
                "Resource": "*"
            }
        ]
    }`
});

export const ec2RolesAndPolicies = role;