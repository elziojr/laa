const createIndexes = (elasticSearchHost: string, elasticSearchPort: number) => {
    return `curl -X PUT '${elasticSearchHost}:${elasticSearchPort}/access' --header 'Content-Type: application/json' --data-raw '{"mappings": {"properties": {"url": {"type":  "keyword"}}}}'`;
}

export const create = createIndexes;