import * as aws from "@pulumi/aws";
import { Subnet, SecurityGroup } from "@pulumi/awsx/ec2";
import { elasticSearchDomainPolicy } from "../roles/elasticSearchDomainPolicy";

const create = (securityGroup: SecurityGroup, subnet: Subnet) => {
    const elasticsearch = new aws.elasticsearch.Domain("elasticSearchDomain", {
        ebsOptions: {
            ebsEnabled: true,
            volumeSize: 10,
            volumeType: "standard"
        },
        clusterConfig: {
            instanceType: "t2.small.elasticsearch"
        },
        elasticsearchVersion: "7.7",
        snapshotOptions: {
            automatedSnapshotStartHour: 23,
        },
        tags: {
            Domain: "ElasticDomain",
        },
        vpcOptions: {
            subnetIds: [subnet.id],
            securityGroupIds: [securityGroup.id]
        }
    });

    elasticSearchDomainPolicy(elasticsearch);
    return elasticsearch;
}

export const createInstance = create;