import * as aws from "@pulumi/aws";
import { FileAsset } from "@pulumi/pulumi/asset";
import { Bucket } from "@pulumi/aws/s3";

const create = (bucketName: string) => {
    const bucket = new aws.s3.Bucket(bucketName, {
        acl: "private",
        tags: {
            Environment: "Dev",
            Name: bucketName,
        }
    });

    return bucket;
};

const createBucketObject = (objectName: string, buecket: Bucket, objectDir: string, fileName: string) => {
    return new aws.s3.BucketObject(objectName, {
        bucket: buecket,
        key: fileName,
        source: new FileAsset(objectDir)
    });
};

export const createInstance = create;
export const createObject = createBucketObject;