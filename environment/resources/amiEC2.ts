import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import { Subnet, SecurityGroup } from "@pulumi/awsx/ec2";
import { InstanceType } from "@pulumi/aws/ec2";
import { ec2RolesAndPolicies } from "../roles/ec2RolesAndPolicies";

const ami = pulumi.output(aws.getAmi({ // imagem AMI de uma máquina EC2 default com 
    filters: [{
      name: "name",
      values: ["amzn-ami-hvm-*"],
    }],
    owners: ["137112412989"], // Owner ID Amazon
    mostRecent: true
}));

const ec2IamProfile = new aws.iam.InstanceProfile("ec2AccessS3AndElasticSearchProfile", {
    role: ec2RolesAndPolicies.name
});

const create = (uniqueName: string, instanceType: InstanceType, securityGroup: SecurityGroup, subnet: Subnet, userData: string)  => {
    const ec2 = new aws.ec2.Instance(uniqueName, {
        ami: ami.id,
        instanceType: instanceType,
        subnetId: subnet.subnet.id,
        availabilityZone: subnet.subnet.availabilityZone,
        vpcSecurityGroupIds: [ securityGroup.id ],
        iamInstanceProfile: ec2IamProfile,
        userData: userData
    });

    return ec2;
}

export const createInstance = create;
