async function blockPromise(promise: Promise<any>) {
    return await promise;
}

export const awaitPromise = blockPromise;